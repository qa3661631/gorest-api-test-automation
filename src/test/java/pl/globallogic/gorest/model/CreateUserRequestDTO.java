package pl.globallogic.gorest.model;

public record CreateUserRequestDTO(String name, String email, String gender, String status) {
}
